import React from 'react';
import ReactDOM from 'react-dom';
import RoutesContainer from './containers/RoutesContainer';

import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import initStore from './store/initStore';

const store = configureStore(initStore);

ReactDOM.render(
  <Provider store={store}>
    <RoutesContainer/>
  </Provider>,
  document.getElementById("interactive-app")
);