
const initStore = {
  page: {
    flowId: 0,
    title: 'Заголовок',
    moderator: false,
    needUpdate: true
  },
  directories: {
    isUpdated: false,
    flowStatuses: [],
    speakerStatuses: [],
    questionTypes: []
  },
  interactive: {
    isUpdated: false,
    flow: null,
    program: null,
    speakers: null,
    listeners: []
  },
  online: {
    isFetching: false,
    isAnswersSending: false,
    speakerOrders: [],
    speakerOrderId: null,
    flowStatusId: 0,
    questions: [],
    answers: [],
    action: null
  },
  questionForSpeaker: {
    id: 0,
    isEditing: true,
    isSending: false,
    isUpdated: false,
    status: '',
    text: '',
    answer: ''
  },
  moderatorSpeakers: {
    isFetching: false,
    isFetchingVote: false,
    prepareSpeakerOrderId: 0,
    prepareSpeakerOrderStatusId: 0,
  },
  moderatorQuestions: {
    listeners: [],
    questions: []
  },
  collections: {
    programs: [],
    flows: [],
  },
  listenersQuestions: {
    isFetching: true,
    questions: [],
    listeners: [],
    votes: []
  },
  statistic: {
    isFetching: false,
    items: []
  }
};

export default initStore;