import React from 'react';
import ListenersQuestionContainer from './actions/listenersQuestions/ListenersQuestionContainer';
import StatisticContainer from './actions/statistic/StatisticContainer';
import QuestionsContainer from './actions/speakerQuestions/QuestionsContainer';
import QuestionForSpeakerContainer from './questionForSpeaker/QuestionForSpeakerContainer';

export default class OnlineActionsFactory {
  static create(type) {
    switch (type) {
      case 'active':
        return (
          <div>
            <QuestionsContainer/>
            <QuestionForSpeakerContainer/>
          </div>
        );
        break;
      case 'listeners_questions':
        return <ListenersQuestionContainer/>;
        break;
      case 'statistic':
        return <StatisticContainer/>;
        break;
      default:
        return null;
        break;
    }
  }
};