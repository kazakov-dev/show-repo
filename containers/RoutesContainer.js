import React from 'react';
import ListLoader from "./loaders/ListLoader";
import InteractiveLoader from "./loaders/IteractiveLoader";
import {Loader, Segment} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {fetchDirectories} from './../actions/directories';

import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';

class MainContainer extends React.Component {
  loadData() {
    const {dispatch} = this.props;
    dispatch(fetchDirectories());
  }

  renderRouter() {
    return(
      <Router basename="/interactive">
        <div>
          <Route path="/" exact component={ListLoader}/>
          <Route path="/:flowId" component={InteractiveLoader}/>
        </div>
      </Router>
    );
  }

  render() {
    const {directoriesIsUpdated} = this.props;
    this.loadData();

    return (
      <div>
        {!directoriesIsUpdated ? (
          <Loader content='Загрузка приложения' inline="centered" active style={{height: "200px"}}/>
        ) : (
          this.renderRouter()
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  directoriesIsUpdated: state.directories.isUpdated
});

MainContainer = connect(mapStateToProps)(MainContainer);

export default MainContainer;