import React from 'react';
import Speaker from './../components/speaker/Speaker';
import OnlineActionsFactory from './OnlineActionsFactory';
import {connect} from 'react-redux';

class OnlineContainer extends React.Component {
  render() {
    const {speakerOrderId, speakerOrders, speakers, action} = this.props;
    const speakerOrder = speakerOrders.find(speakerOrder => speakerOrder.id === speakerOrderId);

    if (!speakerOrder) {
      return null;
    }

    const speaker = speakers.find(speaker => speaker.id === speakerOrder.speakerId);

    return (
      <div>
        <h2>{speakerOrder.name}</h2>
        <Speaker speaker={speaker}/>
        <p style={{margin: "20px 0"}} dangerouslySetInnerHTML={{__html: speakerOrder.description}}/>

        {OnlineActionsFactory.create(action)}

      </div>
    );
  }
}

const mapStateToProps = state => ({
  flow: state.interactive.flow,
  speakers: state.interactive.speakers,
  speakerOrders: state.online.speakerOrders,
  speakerOrderId: state.online.speakerOrderId,
  action: state.online.action
});

OnlineContainer = connect(mapStateToProps)(OnlineContainer);

export default OnlineContainer;