import React from 'react';
import {Segment} from 'semantic-ui-react';
import MenuContainer from './moderator/MenuContainer';
import SpeakerContainer from './moderator/SpeakersContainer';
import QuestionsContainer from './moderator/QuestionsContainer';

class ControlsContainer extends React.Component {
  render() {
    return (
      <Segment style={{marginTop: 40}} color='orange'>
        <h3>Управление конференцией</h3>
        <MenuContainer
          speakersComponent={<SpeakerContainer/>}
          questionsComponent={<QuestionsContainer/>}
        />
      </Segment>
    );
  }
}

export default ControlsContainer;