import React from 'react';
import QuestionFactory from './QuestionFactory';
import {connect} from 'react-redux';
import {Loader} from 'semantic-ui-react';

class QuestionsContainer extends React.Component {
  renderQuestions() {
    const {questions, answers, questionTypes} = this.props;

    return questions
      .map((question) => {
        const questionType = questionTypes.find(questionType => question.questionTypeId === questionType.id);
        const questionAnswers = answers.filter(answer => question.id === answer.questionId);
        return QuestionFactory.create(questionType.code, question, questionAnswers);
      });
  }

  showLoader() {
    const {isAnswersSending} = this.props;

    if (isAnswersSending) {
      return <Loader active inline size="tiny" style={{fontSize: "12px", marginLeft: "20px"}}/>;
    } else {
      return null;
    }
  }

  render() {
    return (
      <div>
        <h3>
          Вопросы
          {this.showLoader()}
        </h3>
        <div>
          {this.renderQuestions()}
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  isAnswersSending: state.online.isAnswersSending,
  questions: state.online.questions,
  answers: state.online.answers,
  questionTypes: state.directories.questionTypes
});

QuestionsContainer = connect(mapStateToProps)(QuestionsContainer);

export default QuestionsContainer;