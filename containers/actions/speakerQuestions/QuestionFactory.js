import React from 'react';
import MultipleAnswer from './../../../components/speaker/questions/MultipleAnswer';
import SingleAnswer from './../../../components/speaker/questions/SingleAnswer';
import RangeAnswer from './../../../components/speaker/questions/RangeAnswer';

export default class OnlineActionsFactory {
  static create(type, question, answers) {
    switch (type) {
      case 'single_answer':
        return <SingleAnswer key={question.id} question={question} answers={answers}/>;
        break;
      case 'multiple_answer':
        return <MultipleAnswer key={question.id} question={question} answers={answers}/>;
        break;
      case 'int_range':
        return <RangeAnswer key={question.id} question={question} answers={answers}/>;
        break;
      default:
        return null;
        break;
    }
  }
};