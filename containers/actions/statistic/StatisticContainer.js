import React, {Component, PureComponent} from 'react';
import {connect} from 'react-redux';
import Statistic from './Statistic';
import {fetchStatistic} from "../../../actions/statistic";
import {Loader} from 'semantic-ui-react';


class StatisticContainer extends PureComponent {
  componentWillMount() {
    const {dispatch, flow} = this.props;
    dispatch(fetchStatistic(flow.id));
  }

  render() {
    const {isFetching, items, questionTypes} = this.props;

  	return (
      <div className="statistic-wrapper">
        <div><h3>Статистика</h3></div>
        {isFetching ? (
          <Loader content='Загрузка' inline="centered" active style={{height: "200px"}}/>
        ) : (
          <Statistic items={items} questionTypes={questionTypes}/>
        )}
      </div>
  	);
  }
}

const mapStateToProps = state => ({
  isFetching: state.statistic.isFetching,
  items: state.statistic.items,
  flow: state.interactive.flow,
  questionTypes: state.directories.questionTypes
});

StatisticContainer = connect(mapStateToProps)(StatisticContainer);

export default StatisticContainer;