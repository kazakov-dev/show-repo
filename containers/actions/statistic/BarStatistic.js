import React, {Component, PureComponent} from 'react';

class BarStatistic extends PureComponent {
  renderAnswers() {
    const {answers, totalAnswers} = this.props;

    return answers.map((answer, key) => {
      const count = answer.count;
      let percent = 0;

      if (totalAnswers > 0) {
        percent = count * 100 / totalAnswers;
      }

      return (
        <div className="statistic-answer-wrapper" key={key}>
          <div>{answer.text}</div>
          <div className="statistic-answer">
            <div className="statistic-answer__bar">
              <div className="statistic-answer__bar-value">{count}</div>
              <div className="statistic-answer__bar-colored-value" style={{width: `${percent}%`}}/>
            </div>
            <div className="statistic-answer__value">{percent.toFixed(1)}%</div>
          </div>
        </div>
      );
    });
  }

  render() {
    const {question} = this.props;

  	return (
  	  <div className="statistic-item">
        <div className="statistic-item__title">{question.name}</div>
        <div>
          {this.renderAnswers()}
        </div>
      </div>
  	);
  }
}

export default BarStatistic;