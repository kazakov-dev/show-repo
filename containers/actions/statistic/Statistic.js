import React, {Component, PureComponent} from 'react';
import QuestionFactory from './QuestionFactory';

class Statistic extends PureComponent {

  renderQuestions() {
    const {items, questionTypes} = this.props;

    return items.map((item) => {
      const {question} = item;
      const questionId = question.id;
      const questionType = questionTypes.find(questionType => question.questionTypeId === questionType.id);

      return QuestionFactory.create(questionType.code, questionId, item);
    });
  }

  render() {
    return (
      <div>
        {this.renderQuestions()}
      </div>
    );
  }
}

export default Statistic;