import React from 'react';
import BarStatistic from './BarStatistic';

export default class QuestionFactory {
  static create(type, questionId, item) {
    switch (type) {
      case 'single_answer':
      case 'multiple_answer':
        return <BarStatistic key={questionId} {...item}/>;
        break;
      case 'int_range':
      default:
        return null;
        break;
    }
  }
};