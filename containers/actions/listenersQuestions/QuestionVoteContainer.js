import React, {Component, PureComponent} from 'react';
import {Button} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {toggleVote} from "../../../actions/listenersQuestions";

class QuestionVoteContainer extends Component {
  handleClick = () => {
    const {dispatch, question} = this.props;
    dispatch(toggleVote(question.id, question.speakerOrderId));
  };

  render() {
    const {vote} = this.props;
    const count = (vote) ? vote.likes : 0;
    const isLiked = (vote) ? vote.isCurrentUserLiked : false;
    const color = isLiked ? 'blue' : 'grey';

  	return (
  	  <div>
        <Button
          onClick={this.handleClick}
          size="tiny"
          color={color}
          content='Нравится'
          icon='thumbs up'
          label={{ basic: true, color: color, pointing: 'left', content: count }}
        />
      </div>
  	);
  }
}

const mapStateToProps = state => ({
  votes: state.listenersQuestions.votes
});

QuestionVoteContainer = connect(mapStateToProps)(QuestionVoteContainer);

export default QuestionVoteContainer;