import React, {Component, PureComponent} from 'react';
import QuestionVoteContainer from './QuestionVoteContainer';
import {Message} from 'semantic-ui-react';

const Listener = ({listener}) => {
  return (
    <div className="listener-question__listener">
      <div className="listener-question__listener-title">{listener.name} {listener.lastName}, {listener.company}</div>
      <div>{listener.position}</div>
    </div>

  );
};


class ListenerQuestion extends PureComponent {
  render() {
    const {question, vote, listener} = this.props;

  	return (
  	  <div className="listener-question">
        <Message className="listener-question__body">
          <div dangerouslySetInnerHTML={{__html: question.text}}/>
        </Message>
        <div className="listener-question__footer">
          <QuestionVoteContainer vote={vote} question={question}/>
          <Listener listener={listener}/>
        </div>
  	  </div>
  	);
  }
}

export default ListenerQuestion;