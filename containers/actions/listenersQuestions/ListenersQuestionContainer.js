import React, {PureComponent} from 'react';
import {Loader} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {fetchListenersQuestions} from "../../../actions/listenersQuestions";
import QuestionList from './QuestionsList';

const mapStateToPropsQuestions = state => ({
  listeners: state.listenersQuestions.listeners,
  questions: state.listenersQuestions.questions,
  votes: state.listenersQuestions.votes,
});

const QuestionsListContainer = connect(mapStateToPropsQuestions)(QuestionList);

class ListenersQuestionContainer extends PureComponent {
	componentDidMount() {
	  const {dispatch, speakerOrderId} = this.props;
    dispatch(fetchListenersQuestions(speakerOrderId));
	}

  render() {
	  const {isFetching} = this.props;

  	return (
  	  <div className="listeners-questions">
        <div><h3>Вопросы слушателей</h3></div>
        {isFetching ? (
          <Loader content='Загрузка' inline="centered" active style={{height: "200px"}}/>
        ) : (
          <QuestionsListContainer/>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isFetching: state.listenersQuestions.isFetching,
  speakerOrderId: state.online.speakerOrderId
});

ListenersQuestionContainer = connect(mapStateToProps)(ListenersQuestionContainer);

export default ListenersQuestionContainer;