import React, {Component} from 'react';
import ListenerQuestion from './ListenerQuestion'

class QuestionList extends Component {
	renderList() {
    const {questions, listeners, votes} = this.props;

    return questions.map((question) => {
      const vote = votes.find(vote => vote.questionForSpeakerId === question.id);
      const listener = listeners.find(listener => listener.id === question.userId);
    	return (<ListenerQuestion key={question.id} question={question} vote={vote} listener={listener}/>)
		});
	}

  render() {
  	const {questions} = this.props;

  	return (
      <div>
				{questions.length === 0 ? (
					<div>Пока не было задано ни одного вопроса</div>
				) : (
					this.renderList()
				)}
			</div>
  	);
  }
}

export default QuestionList;