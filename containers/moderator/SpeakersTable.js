import React, {Component, PureComponent} from 'react';
import {Table} from 'semantic-ui-react';
import SpeakerRow from "./SpeakerRow";

class SpeakersTable extends PureComponent {
	renderTable() {
		const {speakers, speakerOrders, speakerStatuses, speakerOrderId} = this.props;

		return speakerOrders.map(speakerOrder => {
			const speaker = speakers.find(speaker => speaker.id === speakerOrder.speakerId);
			const status = speakerStatuses.find(speakerStatus => speakerStatus.id === speakerOrder.speakerOrderStatusId);

			return (
				<SpeakerRow key={speakerOrder.id} {...{speakerOrder, speaker, status, speakerOrderId, speakerStatuses}}/>
			);
		});
	}

  render() {
  	return (
			<Table basic='very'>
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>Спикер</Table.HeaderCell>
						<Table.HeaderCell>Статус</Table.HeaderCell>
						<Table.HeaderCell/>
					</Table.Row>
				</Table.Header>
					<Table.Body>
            {this.renderTable()}
					</Table.Body>
			</Table>
  	);
  }
}

export default SpeakersTable;