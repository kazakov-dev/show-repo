import React, {Component, PureComponent} from 'react';
import {connect} from 'react-redux';
import QuestionsTable from "./QuestionsTable";
import {fetchModeratorQuestions, sendQuestionStatus} from "../../actions/moderatorQuestions";


class QuestionsContainer extends PureComponent {
  componentDidMount() {
    const {fetchModeratorQuestions, speakerOrderId} = this.props;
    fetchModeratorQuestions(speakerOrderId);

    this.fetchInterval = setInterval(() => {
      fetchModeratorQuestions(speakerOrderId);
    }, 5000);
  }

  componentWillUnmount() {
    clearInterval(this.fetchInterval);
  }

  render() {
  	return (
      <QuestionsTable {...this.props}/>
  	);
  }
}

const mapStateToProps = state => ({
  speakerOrderId: state.online.speakerOrderId,
  questions: state.moderatorQuestions.questions,
  listeners: state.moderatorQuestions.listeners,
  isFetching: state.moderatorQuestions.isFetching
});

const mapDispatchToProps = dispatch => ({
  applyQuestion: (questionId) => dispatch(sendQuestionStatus(questionId, 'apply')),
  declineQuestion: (questionId) => dispatch(sendQuestionStatus(questionId, 'decline')),
  fetchModeratorQuestions: (speakerOrderId) => dispatch(fetchModeratorQuestions(speakerOrderId))
});

QuestionsContainer = connect(mapStateToProps, mapDispatchToProps)(QuestionsContainer);

export default QuestionsContainer;