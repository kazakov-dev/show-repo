import React, {Component} from 'react';
import QuestionRow from './QuestionRow';
import {Table} from 'semantic-ui-react';

class QuestionsTable extends Component {
  renderTable = () => {
    const {questions, listeners, applyQuestion, declineQuestion} = this.props;

    return questions.map(question => {
      const listener = listeners.find(listener => question.userId === listener.id);

      return (
        <QuestionRow
          key={question.id}
          question={question}
          listener={listener}
          applyQuestion={applyQuestion}
          declineQuestion={declineQuestion}
        />
      );
    });
  };

  render() {
    return (
      <Table basic='very'>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Вопрос</Table.HeaderCell>
            <Table.HeaderCell>Управление</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {this.renderTable()}
        </Table.Body>
      </Table>
    );
  }
}

export default QuestionsTable;