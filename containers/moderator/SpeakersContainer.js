import React, {Component, PureComponent} from 'react';
import {connect} from 'react-redux';
import SpeakersTable from './SpeakersTable';

const mapStateToProps = state => ({
	speakers: state.interactive.speakers,
  speakerOrders: state.online.speakerOrders,
  speakerOrderId: state.online.speakerOrderId,
  speakerStatuses: state.directories.speakerStatuses
});

const SpeakerTableContainer = connect(mapStateToProps)(SpeakersTable);

class SpeakersContainer extends Component {
  render() {
  	return (
  	  <div>
				<SpeakerTableContainer/>
			</div>
  	);
  }
}

export default SpeakersContainer;