import React, {Component, PureComponent} from 'react';
import {Menu, Loader} from 'semantic-ui-react';
import {connect} from 'react-redux';

class MenuContainer extends PureComponent {
  state = {
    activeItem: 'speakers'
  };

  handleItemClick = (e, {name}) => this.setState({activeItem: name});

  renderContent() {
    const {activeItem} = this.state;
    const {speakersComponent, questionsComponent} = this.props;

    switch (activeItem) {
      case "speakers":
        return speakersComponent;
        break;
      case "questions":
        return questionsComponent;
        break;
    }
  }

  render() {
    const {activeItem} = this.state;
    const {isModeratorQuestionFetching, action} = this.props;

    // TODO: Переделать, костыль!
  	return (
      <div>
        <Menu pointing secondary>
          <Menu.Item
            name='speakers'
            active={activeItem === 'speakers'}
            onClick={this.handleItemClick}>
            Спикеры
          </Menu.Item>
          {action === 'active' &&
            <Menu.Item
              name='questions'
              active={activeItem === 'questions'}
              onClick={this.handleItemClick}>
              Вопросы слушателей
              <Loader inline active={isModeratorQuestionFetching} size="tiny" style={{marginLeft: 10, height: 14}}/>
            </Menu.Item>
          }
        </Menu>
        <div>
          {this.renderContent()}
        </div>
      </div>
  	);
  }
}

const mapStateToProps = state => ({
  isModeratorQuestionFetching: state.moderatorQuestions.isFetching,
  action: state.online.action
});

MenuContainer = connect(mapStateToProps)(MenuContainer);

export default MenuContainer;