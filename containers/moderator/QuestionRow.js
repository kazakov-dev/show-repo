import React, {Component, PureComponent} from 'react';
import {Table, Button} from 'semantic-ui-react';

class QuestionRow extends Component {
  handleApply = () => {
    const {applyQuestion, question} = this.props;
    applyQuestion(question.id);
  };
  handleDecline = () => {
    const {declineQuestion, question} = this.props;
    declineQuestion(question.id);
  };

  render() {
  	const {question, listener} = this.props;
  	const applyStatus = question.status === 'apply';
  	const declineStatus = question.status === 'decline';

  	return (
			<Table.Row>
				<Table.Cell className="moderator-question">
					<div className="moderator-question__listener">{listener.name} {listener.lastName} / {listener.company}, {listener.position} </div>
					<div dangerouslySetInnerHTML={{__html: question.text}}/>
				</Table.Cell>
				<Table.Cell className="moderator-question__controls">
          <Button
						content='Принять'
						icon='check'
            labelPosition='right'
            color='green'
            active={applyStatus}
            onClick={this.handleApply}
            inverted={!applyStatus}/>
          <Button
            content='Отклонить'
            color="red"
            active={declineStatus}
            onClick={this.handleDecline}
            inverted={!declineStatus}/>
				</Table.Cell>
			</Table.Row>
  	);
  }
}

export default QuestionRow;