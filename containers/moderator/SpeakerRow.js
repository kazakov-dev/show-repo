import React from 'react';
import {connect} from 'react-redux';
import {Table, Dropdown, Button, List, Image} from 'semantic-ui-react';
import {prepareSpeakerStatusChange, postSpeakerStatusChange} from "../../actions/moderatorSpeakers";

class SpeakerRow extends React.Component {
  state = {
    renderApplyButton: false
  };

  renderStatusDropDown() {
    const {status, speakerStatuses, speakerOrder, prepareSpeakerOrderStatusId, prepareSpeakerOrderId} = this.props;

    const options = speakerStatuses.map(speakerStatus => {
      return {
        key: speakerStatus.id,
        value: speakerStatus.id,
        text: speakerStatus.name
      }
    });

    let value;
    if (prepareSpeakerOrderId !== speakerOrder.id) {
      value = status.id;
    } else {
      value = prepareSpeakerOrderStatusId;
    }

    return (
      <Dropdown
        fluid
        search
        selection
        upward
        options={options}
        value={value}
        onChange={this.handleChangeStatus}/>
    );
  }

  handleChangeStatus = (event, el) => {
    const nextStatusId = el.value;
    const {dispatch, speakerOrder} = this.props;

    dispatch(prepareSpeakerStatusChange(speakerOrder.id, nextStatusId));
  };

  componentWillReceiveProps(nextProps) {
    const {status, speakerOrder} = this.props;
    const {prepareSpeakerOrderId, prepareSpeakerOrderStatusId} = nextProps;

    this.setState({
      renderApplyButton: prepareSpeakerOrderStatusId !== status.id && prepareSpeakerOrderId === speakerOrder.id
    });
  }

  handleApply = (event, el) => {
    const {dispatch, prepareSpeakerOrderId, prepareSpeakerOrderStatusId} = this.props;
    dispatch(postSpeakerStatusChange(prepareSpeakerOrderId, prepareSpeakerOrderStatusId));
  };

  render() {
    const {speaker, speakerOrder, speakerOrderId, isFetching} = this.props;
    const active = speakerOrderId === speakerOrder.id;

    return (
      <Table.Row key={speaker.id} positive={active}>
        <Table.Cell>
          <List>
            <div className="speaker-item">
              <Image avatar src={speaker.image} className="speaker-item__image"/>
              <List.Content>
                <List.Header className="speaker-item__header">{speakerOrder.name}</List.Header>
                <List.Description>{speaker.name}, {speaker.position}</List.Description>
              </List.Content>
            </div>
          </List>
        </Table.Cell>
        <Table.Cell>
          <div style={{width: 200}}>
            {this.renderStatusDropDown()}
          </div>
        </Table.Cell>
        <Table.Cell>
          <div style={{width: 150}}>
            {this.state.renderApplyButton && <Button onClick={this.handleApply} loading={isFetching}>Применить</Button>}
          </div>
        </Table.Cell>
      </Table.Row>
    );
  }
}

const mapStateToProps = state => ({
  isFetching: state.moderatorSpeakers.isFetching,
  prepareSpeakerOrderId: state.moderatorSpeakers.prepareSpeakerOrderId,
  prepareSpeakerOrderStatusId: state.moderatorSpeakers.prepareSpeakerOrderStatusId,
});

SpeakerRow = connect(mapStateToProps)(SpeakerRow);

export default SpeakerRow;