import React from 'react';
import {connect} from 'react-redux';
import {Segment} from 'semantic-ui-react';
import Title from './components/Title';
import QuestionForm from './components/QuestionForm';
import Question from './components/Question';
import {fetchQuestion} from "../../actions/questionForSpeaker";

const mapStateToProps = state => ({
  isSending: state.questionForSpeaker.isSending,
  isUpdated: state.questionForSpeaker.isUpdated,
  text: state.questionForSpeaker.text,
  status: state.questionForSpeaker.status,
  action: state.online.action,
  speakerOrderId: state.online.speakerOrderId
});

const QuestionFormConnect = connect(mapStateToProps)(QuestionForm);
const QuestionConnect = connect(mapStateToProps)(Question);

class QuestionForSpeaker extends React.Component {
  componentDidMount() {
    const {dispatch, speakerOrderId} = this.props;
    dispatch(fetchQuestion(speakerOrderId));
  }

  componentWillReceiveProps(nextProps) {
    const {dispatch, speakerOrderId} = this.props;
    const nextSpeakerOrderId = nextProps.speakerOrderId;

    if (speakerOrderId !== nextSpeakerOrderId) {
      dispatch(fetchQuestion(nextSpeakerOrderId));
    }
  }

  render() {
    const {isEditing, status, id} = this.props;
    return (
      <div>
        <Title/>
        {isEditing && ((status !== 'decline' && status !== 'apply') || id === 0)  ? (
          <QuestionFormConnect/>
        ) : (
          <QuestionConnect/>
        )}
      </div>
    );
  }
}

const mapStateToPropsContainer = state => ({
  id: state.questionForSpeaker.id,
  isEditing: state.questionForSpeaker.isEditing,
  status: state.questionForSpeaker.status,
  speakerOrderId: state.online.speakerOrderId
});

QuestionForSpeaker = connect(mapStateToPropsContainer)(QuestionForSpeaker);

export default QuestionForSpeaker;