import React, {Component, PureComponent} from 'react';
import {Form, TextArea, Button, Label} from 'semantic-ui-react';
import {sendQuestion, disableEditing} from "../../../actions/questionForSpeaker";

class QuestionForm extends PureComponent {
  minCharCount = 50;

  state = {
    text: ''
  };

  componentDidMount() {
    const {text} = this.props;
    let textWithOutHtml = text.replace(/<br\s?\/>/g, "\r");

    this.setState({text: textWithOutHtml});
  }

  handleChange = (event, el) => {
    const dirtyText = el.value;
    let text = dirtyText.replace(/\s\s/g, " ");
    text = text.replace(/\r\r\r?/g, "\r");

    this.setState({text});
  };

  handleSend = () => {
    const {dispatch, speakerOrderId} = this.props;
    const charLeft = this.getCharLeft();
    const {text} = this.state;

    if (charLeft > 0) {
      return;
    }

    const textWithHtml = text.replace(/\r/g, "<br />");

    dispatch(sendQuestion(speakerOrderId, textWithHtml));
  };

  handleCancel = () => {
    const {dispatch} = this.props;
    dispatch(disableEditing());
  };

  renderCancelButton() {
    const {text} = this.props;

    if (text !== '') {
      return <Button onClick={this.handleCancel} floated="right">Назад</Button>;
    } else {
      return null;
    }
  }

  getCharLeft() {
    const {text} = this.state;
    return this.minCharCount - text.length;
  }

  renderCharCounter() {
    const charLeft = this.getCharLeft();

    if (charLeft <= 0) {
      return null;
    }

    return (
      <Label>
        Осталось набрать
        <Label.Detail>{charLeft} знаков</Label.Detail>
      </Label>
    );
  }

  render() {
    const {isSending} = this.props;
    const {text} = this.state;

  	return (
      <Form>
        <div>
          {this.renderCharCounter()}
        </div>
        <TextArea placeholder='' style={{ minHeight: 100 , margin: "10px 0"}} onChange={this.handleChange} value={text}/>
        <div>
          <Button
            primary
            onClick={this.handleSend}
            loading={isSending}
            disabled={isSending}
          >Отправить</Button>
          {this.renderCancelButton()}
        </div>


      </Form>
  	);
  }
}

export default QuestionForm;