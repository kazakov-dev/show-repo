import React, {Component, PureComponent} from 'react';
import StatusBar from './StatusBar';
import {Message, Button} from 'semantic-ui-react';
import {fetchQuestion, enableEditing} from "../../../actions/questionForSpeaker";

class Question extends PureComponent {
  fetchInterval = null;

  componentDidMount() {
    const {dispatch, speakerOrderId} = this.props;
    this.fetchInterval = setInterval(() => {
      dispatch(fetchQuestion(speakerOrderId));
    }, 20000);
  }

  componentWillUnmount() {
    clearInterval(this.fetchInterval);
  }

  handleClick = () => {
    const {dispatch} = this.props;

    dispatch(enableEditing());
  };

  render() {
    const {status, text, action} = this.props;

  	return (
  	  <div>
        <Message floating style={{wordWrap: "break-word"}}>
          <div dangerouslySetInnerHTML={{__html: text}}/>
        </Message>
        {status === 'await' && action === 'active' &&
          <Button size="small" onClick={this.handleClick}>Редактировать</Button>
        }
        <StatusBar status={status} style={{marginLeft: 10}}/>
      </div>
  	);
  }
}

export default Question;