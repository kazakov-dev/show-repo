import React, {Component, PureComponent} from 'react';
import {Label} from 'semantic-ui-react';

class StatusBar extends PureComponent {
  render() {
  	const {status} = this.props;

  	switch (status) {
			case "await":
				return <Label color="yellow">Ожидает модерацию</Label>;
				break;
      case "apply":
        return <Label color="green">Ваш вопрос принят</Label>;
        break;
      case "decline":
        return <Label color="orange">Ваш вопрос отклонен</Label>;
        break;
      default:
        return null;
        break;
		}
  }
}

export default StatusBar;