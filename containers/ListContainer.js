import React from 'react';
import {connect} from 'react-redux';
import FlowsList from "../components/FlowsList";
import {Header} from 'semantic-ui-react';

class ListContainer extends React.Component {
  render() {
    return (
      <div>
        <Header as="h1">Интерактив</Header>
        <FlowsList {...this.props}/>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  flows: state.collections.flows,
  programs: state.collections.programs,
  statuses: state.directories.flowStatuses
});

ListContainer = connect(mapStateToProps)(ListContainer);

export default ListContainer;