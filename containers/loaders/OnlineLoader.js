import React from 'react';
import {connect} from 'react-redux';
import {Loader} from 'semantic-ui-react';
import OnlineContainer from "./../OnlineContainer";
import {fetchOnline} from "../../actions/online";

class OnlineLoader extends React.Component {
  componentDidMount() {
    const {dispatch, flowId} = this.props;

    dispatch(fetchOnline(flowId));
    this.updateInterval = setInterval(() => {
      dispatch(fetchOnline(flowId));
    }, 10000);
  }

  componentWillUnmount() {
    clearInterval(this.updateInterval);
  }

  shouldComponentUpdate(props) {
    const {isFetching} = props;
    return !isFetching;
  }

  render() {
    return <OnlineContainer/>
  }
}

const mapStateToProps = state => ({
  isFetching: state.online.isFetching
});

OnlineLoader = connect(mapStateToProps)(OnlineLoader);

export default OnlineLoader;