import React from 'react';
import {connect} from 'react-redux';
import {Loader} from 'semantic-ui-react';
import axios from 'axios';
import ListContainer from './../ListContainer';

import {showListPage} from './../../actions/page';

class ListLoader extends React.Component {
  loadData() {
    const {dispatch} = this.props;

    axios.get('/api/interactive/getInfo', {
      withCredentials: true
    })
      .then(({data}) => {
        dispatch(showListPage({
          collections: {
            flows: data.flows,
            programs: data.programs
          },
          moderator: data.moderator
        }))
      })
      .catch(err => {
        alert('Что-то пошло не так');
      });
  }

  render() {
    const {needUpdate} = this.props;

    this.loadData();

    return (
      <div>
        {needUpdate ? (
          <Loader content='Загрузка' inline="centered" active style={{height: "200px"}}/>
        ) : (
          <ListContainer/>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  needUpdate: state.page.needUpdate
});

ListLoader = connect(mapStateToProps)(ListLoader);

export default ListLoader;