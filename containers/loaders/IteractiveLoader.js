import React from 'react';
import {connect} from 'react-redux';
import {Loader} from 'semantic-ui-react';
import NavContainer from "./../NavContainer";
import {fetchInteractive} from "../../actions/interactive";

class InteractiveLoader extends React.Component {
  componentDidMount() {
    const {dispatch, match} = this.props;
    const flowId = parseInt(match.params.flowId) || 0;
    dispatch(fetchInteractive(flowId));
  }

  render() {
    const {interactiveIsUpdated} = this.props;

    return (
      <div>
        {!interactiveIsUpdated ? (
          <Loader content='Загрука конференции' inline="centered" active style={{height: "200px"}}/>
        ) : (
          <NavContainer/>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  interactiveIsUpdated: state.interactive.isUpdated
});

InteractiveLoader = connect(mapStateToProps)(InteractiveLoader);

export default InteractiveLoader;