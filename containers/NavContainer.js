import React from 'react';
import {connect} from 'react-redux';
import {setTitle} from "../actions/page";
import ModeratorContainer from './ModeratorContainer';
import FlowBreadcrumb from "../components/FlowBreadcrumb";
import StatusLabel from "../components/StatusLabel";
import SpeakerList from "../components/SpeakerList";
import TabMenu from "../components/TabMenu";
import Program from "../components/Program";
import OnlineLoader from "./loaders/OnlineLoader";

class NavContainer extends React.Component {
  render() {
    const {flow, flowStatus, speakers, program, moderator, isFetching} = this.props;

    if (!flow) {
      return (
        <h3>Интерактив не найден или у Вас нет доступа к нему</h3>
      );
    }

    return (
      <div className="moderator-wrapper">
        <FlowBreadcrumb {...this.props}/>
        <StatusLabel {...flowStatus} style={{margin: "15px 0"}}/>
        <TabMenu
          isFetching={isFetching}
          status={status}
          mainComponent={<OnlineLoader flowId={flow.id}/>}
          speakersComponent={<SpeakerList speakers={speakers}/>}
          programComponent={<Program {...program}/>}
        />
        {moderator && <ModeratorContainer/>}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isFetching: state.online.isFetching,
  flow: state.interactive.flow,
  program: state.interactive.program,
  speakers: state.interactive.speakers,
  moderator: state.interactive.moderator,
  flowStatus: (() => {
    return state.directories.flowStatuses.find(status => status.id === state.online.flowStatusId);
  })()
});

NavContainer = connect(mapStateToProps)(NavContainer);

export default NavContainer;