import {SET_COLLECTIONS} from './../actions/collections';

const collections = (state = {}, action) => {
  let actionWithoutType;
  actionWithoutType = {...action};
  delete actionWithoutType.type;

  switch (action.type) {
    case SET_COLLECTIONS:
      return {...state, ...actionWithoutType};
    default:
      return state;
  }
};

export default collections;