import {REQUEST_INTERACTIVE, RECEIVE_INTERACTIVE} from './../actions/interactive';

const interactive = (state = {}, action) => {
  let actionWithoutType;
  actionWithoutType = {...action};
  delete actionWithoutType.type;

  switch (action.type) {
    case REQUEST_INTERACTIVE:
    case RECEIVE_INTERACTIVE:
      return {...state, ...actionWithoutType};
    default:
      return state;
  }
};

export default interactive;