import {REQUEST_DIRECTORIES, RECEIVE_DIRECTORIES} from './../actions/directories';

const directories = (state = {}, action) => {
  let actionWithoutType;
  actionWithoutType = {...action};
  delete actionWithoutType.type;

  switch (action.type) {
    case RECEIVE_DIRECTORIES:
    case REQUEST_DIRECTORIES:
      return {...state, ...actionWithoutType};
    default:
      return state;
  }
};

export default directories;