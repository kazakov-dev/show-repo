import {REQUEST_LISTENERS_QUESTIONS, RECEIVE_LISTENERS_QUESTIONS, RECEIVE_TOGGLE_VOTE, REQUEST_TOGGLE_VOTE} from './../actions/listenersQuestions';

const listenersQuestions = (state = {}, action) => {
  let actionWithoutType;
  actionWithoutType = {...action};
  delete actionWithoutType.type;

  switch (action.type) {
    case REQUEST_TOGGLE_VOTE:
    case RECEIVE_TOGGLE_VOTE:
    case REQUEST_LISTENERS_QUESTIONS:
    case RECEIVE_LISTENERS_QUESTIONS:
      return {...state, ...actionWithoutType};
    default:
      return state;
  }
};

export default listenersQuestions;