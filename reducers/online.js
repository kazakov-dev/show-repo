import {RECEIVE_ONLINE, REQUEST_ONLINE, REQUEST_ANSWER_UPDATE, ANSWER_UPDATED, SET_SPEAKER} from './../actions/online';

const online = (state = {}, action) => {
  let actionWithoutType;
  actionWithoutType = {...action};
  delete actionWithoutType.type;

  switch (action.type) {
    case REQUEST_ANSWER_UPDATE:
      const answers = action.answers;
      const questionId = action.questionId;
      const prevAnswers = state.answers;

      let nextAnswers = [];
      nextAnswers = prevAnswers.filter(prevAnswer => prevAnswer.questionId !== questionId);
      nextAnswers = [...nextAnswers, ...answers];

      return {...state, isAnswersSending: action.isAnswersSending, answers: nextAnswers};
      break;

    case ANSWER_UPDATED:
    case SET_SPEAKER:
      return {...state, isAnswersSending: action.isAnswersSending};
      break;

    case RECEIVE_ONLINE:
    case REQUEST_ONLINE:
      let {isAnswersSending} = state;

      if (isAnswersSending) {
        return {...state};
      }

      return {...state, ...actionWithoutType};
    default:
      return state;
  }
};

export default online;