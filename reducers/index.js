import { combineReducers } from 'redux';
import page from './page';
import collections from './collections';
import interactive from './interactive';
import directories from './directories';
import online from './online';
import questionForSpeaker from './questionForSpeaker';
import moderatorSpeakers from './moderatorSpeakers';
import listenersQuestions from './listenersQuestions';
import moderatorQuestions from './moderatorQuestions';
import statistic from './statistic';

const reducers = combineReducers({
  page,
  collections,
  interactive,
  directories,
  online,
  questionForSpeaker,
  moderatorSpeakers,
  listenersQuestions,
  moderatorQuestions,
  statistic
});

export default reducers;
