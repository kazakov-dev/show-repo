import {RECEIVE_STATISTIC, REQUEST_STATISTIC} from './../actions/statistic';

const statistic = (state = {}, action) => {
  let actionWithoutType;
  actionWithoutType = {...action};
  delete actionWithoutType.type;

  switch (action.type) {
    case RECEIVE_STATISTIC:
    case REQUEST_STATISTIC:
      return {...state, ...actionWithoutType};
    default:
      return state;
  }
};

export default statistic;