import {SET_TITLE, SET_UPDATE_STATE, SET_MODERATOR} from './../actions/page';

const page = (state = {}, action) => {
  let actionWithoutType;
  actionWithoutType = {...action};
  delete actionWithoutType.type;

  switch (action.type) {
    case SET_UPDATE_STATE:
    case SET_TITLE:
    case SET_MODERATOR:
      return {...state, ...actionWithoutType};
    default:
      return state;
  }
};

export default page;