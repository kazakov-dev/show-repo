import {RECEIVE_MODERATOR_QUESTIONS, REQUEST_MODERATOR_QUESTIONS, SET_QUESTION_STATUS} from './../actions/moderatorQuestions';

const moderatorQuestions = (state = {}, action) => {
  let actionWithoutType;
  actionWithoutType = {...action};
  delete actionWithoutType.type;

  switch (action.type) {
    case SET_QUESTION_STATUS:
      const status = action.status;
      const prevQuestions = state.questions;
      const questions = prevQuestions.map(question => {
        if (question.id === action.questionId) {
          question.status = status;
        }

        return question;
      });

      return {...state, questions};

      break;
    case RECEIVE_MODERATOR_QUESTIONS:
    case REQUEST_MODERATOR_QUESTIONS:
      return {...state, ...actionWithoutType};
    default:
      return state;
  }
};

export default moderatorQuestions;