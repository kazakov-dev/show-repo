import {PREPARE_SPEAKER_STATUS_CHANGE, RECEIVE_SPEAKER_STATUS_CHANGE, REQUEST_SPEAKER_STATUS_CHANGE, REVERT_SPEAKER_STATUS_CHANGE} from './../actions/moderatorSpeakers';

const moderatorSpeakers = (state = {}, action) => {
  let actionWithoutType;
  actionWithoutType = {...action};
  delete actionWithoutType.type;

  switch (action.type) {
    case PREPARE_SPEAKER_STATUS_CHANGE:
    case RECEIVE_SPEAKER_STATUS_CHANGE:
    case REQUEST_SPEAKER_STATUS_CHANGE:
    case REVERT_SPEAKER_STATUS_CHANGE:
      return {...state, ...actionWithoutType};
    default:
      return state;
  }
};

export default moderatorSpeakers;