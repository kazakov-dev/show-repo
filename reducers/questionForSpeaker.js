import {ENABLE_EDITING, QUESTION_SENT, RECEIVE_QUESTION, REQUEST_QUESTION, REQUEST_SENDING_QUESTION, DISABLE_EDITING} from './../actions/questionForSpeaker';

const questionForSpeaker = (state = {}, action) => {
  let actionWithoutType;
  actionWithoutType = {...action};
  delete actionWithoutType.type;

  switch (action.type) {
    case REQUEST_QUESTION:
      // Если редактируем, то запрос на обновление не отправляется
      if (state.isEditing || state.isSending) {
        return {...state};
      }

      return {...state, ...actionWithoutType};
      break;

    case RECEIVE_QUESTION:
    case REQUEST_SENDING_QUESTION:
    case QUESTION_SENT:
    case ENABLE_EDITING:
    case DISABLE_EDITING:
      return {...state, ...actionWithoutType};
    default:
      return state;
  }
};

export default questionForSpeaker;
