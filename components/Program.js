import React from 'react';
import {Image, Grid} from 'semantic-ui-react';


class Program extends React.Component {
  render() {
    const {image, name, description} = this.props;

    return (
      <div className="row">
        <div className="col-md-3">
          <Image src={image} />
        </div>
        <div className="col-md-9">
          <h3>{name}</h3>
          <p dangerouslySetInnerHTML={{__html: description}}/>
        </div>
      </div>
    );
  }
}

export default Program;