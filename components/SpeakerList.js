import React from 'react';
import {List, Grid, Menu, Image, Card} from 'semantic-ui-react';

class SpeakerList extends React.PureComponent {
  renderList() {
    const {speakers} = this.props;
    return speakers.map(speaker => (
      <Card key={speaker.id}>
        <Image src={speaker.image}/>
        <Card.Content>
          <Card.Header>
            {speaker.name}
          </Card.Header>
          <Card.Meta>
            <span className='date'>
              {speaker.position}
            </span>
          </Card.Meta>
          <Card.Description dangerouslySetInnerHTML={{__html: speaker.description}}/>
        </Card.Content>
      </Card>
    ));
  }

  render() {
    return (
      <Card.Group>
        {this.renderList()}
      </Card.Group>
    );
  }
}

export default SpeakerList;