import React from 'react';
import {connect} from 'react-redux';
import { Form, Checkbox } from 'semantic-ui-react';
import {postAnswer} from "../../../actions/online";

class SingleAnswer extends React.Component {
  handleChange = (event, el) => {
    const {question, dispatch} = this.props;
    const questionId = question.id;
    const {value} = el;
    const nextAnswers= [{
        questionId: questionId,
        answerVariantId: value
      }];

    dispatch(postAnswer(nextAnswers, questionId));
  };

  renderVariants() {
    const {question, answers} = this.props;
    const {answerVariants} = question;

    return answerVariants.map(answerVariant => (
      <Form.Field key={answerVariant.id}>
        <Checkbox
          className="question-form question-form--radio"
          value={answerVariant.id}
          checked={!!answers.find(answer => answer.answerVariantId === answerVariant.id)}
          label={answerVariant.name}
          onChange={this.handleChange}
          name={`checkboxRadioGroup-${question.id}`}
          radio
        />
      </Form.Field>
    ));
  }

  render() {
    const {question} = this.props;

    return (
      <div className="question">
        <Form.Field>
          <h5 className="question__title">{question.name}</h5>
        </Form.Field>
        {this.renderVariants()}
      </div>
    );
  }
}

SingleAnswer = connect()(SingleAnswer);

export default SingleAnswer;