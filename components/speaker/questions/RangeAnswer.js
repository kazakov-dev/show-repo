import React from 'react';

class RangeAnswer extends React.Component {
  render() {
    const {question} = this.props;

    return (
      <div>
        <h5 style={{color: "#ccc"}}>{question.name}</h5>
      </div>
    );
  }
}

export default RangeAnswer;