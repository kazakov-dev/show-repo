import React from 'react';
import {Label} from 'semantic-ui-react';

class Speaker extends React.Component {
  render() {
    const {speaker} = this.props;

    return (
      <Label as='a' color='blue' image size="large">
        <img src={speaker.image} />
        {speaker.name}
        <Label.Detail>{speaker.position}</Label.Detail>
      </Label>
    );
  }
} 

export default Speaker;