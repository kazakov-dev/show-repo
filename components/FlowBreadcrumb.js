import React from 'react';
import {Breadcrumb} from 'semantic-ui-react';

class FlowBreadcrumb extends React.Component {
  render() {
    const {program, flow} = this.props;

    return (
      <div>
        <Breadcrumb size="large">
          <Breadcrumb.Section>
            <a href="/interactive">Интерактив</a>
          </Breadcrumb.Section>
          <Breadcrumb.Divider icon='right angle' />
          <Breadcrumb.Section>{program.name} / {flow.name}</Breadcrumb.Section>
        </Breadcrumb>
      </div>
    );
  }
}

export default FlowBreadcrumb;