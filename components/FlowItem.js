import React from 'react';
import {Card, Label} from "semantic-ui-react";
import {Link} from 'react-router-dom';
import StatusLabel from './../components/StatusLabel';



class FlowItem extends React.Component {
  render() {
    const {flow, program, status} = this.props;

    return (
      <div className="col-md-6">
        <Link to={`/${flow.id}`}>
          <Card
            style={{fontWeight: 'normal'}}
            fluid={true}
            //image='https://pp.userapi.com/c7010/v7010658/a136/CZRIu7GrqnY.jpg'
            //image='https://pp.userapi.com/c636429/v636429041/577f0/KX-d4H1isNo.jpg'
            image={program.image}
            header={program.name}
            meta={
              <div className="meta">
                <div>{flow.name}</div>
              </div>
            }
            extra={<StatusLabel code={status.code} name={status.name}/>}
          />
        </Link>
      </div>
    );
  }
}

export default FlowItem;