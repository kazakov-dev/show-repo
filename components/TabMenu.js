import React from 'react';
import {List, Grid, Menu, Segment, Loader} from 'semantic-ui-react';

class TabMenu extends React.Component {
  state = {
    activeItem: 'main'
  };

  handleItemClick = (e, {name}) => this.setState({activeItem: name});

  renderContent() {
    const {activeItem} = this.state;
    const {mainComponent, speakersComponent, programComponent} = this.props;

    // Довольно сильно захардкожено, скорее всего нужно использовать роуты
    switch (activeItem) {
      case "main":
        return mainComponent;
        break;
      case "speakers":
        return speakersComponent;
        break;
      case "program":
        return programComponent;
        break;

    }
  }

  render() {
    const {isFetching} = this.props;
    const {activeItem} = this.state;

    return (
      <div>
        <Menu pointing secondary>
          <Menu.Item name='main' active={activeItem === 'main'} onClick={this.handleItemClick}>
            Online
            <Loader inline active={isFetching} size="tiny" style={{height: 14}}/>
          </Menu.Item>

          <Menu.Item name='speakers' active={activeItem === 'speakers'} onClick={this.handleItemClick}>
            Спикеры
          </Menu.Item>

          <Menu.Item name='program' active={activeItem === 'program'} onClick={this.handleItemClick}>
            Программа
          </Menu.Item>
        </Menu>

        <div>
          {this.renderContent()}
        </div>
      </div>
    );
  }
}

export default TabMenu;


