import React from 'react';
import {Label} from 'semantic-ui-react';

const mapStatusCodeToColor = {
  new_flow: 'red',
  listeners_registration: 'orange',
  speakers_await: 'olive',
  active: 'red'
};

const getStatusColor = (code) => {
  let color = 'teal';

  if (mapStatusCodeToColor.hasOwnProperty(code)) {
    color = mapStatusCodeToColor[code];
  }

  return color;
};

class StatusLabel extends React.PureComponent {
  render() {
    const {style, code, name} = this.props;

    return (
      <Label color={getStatusColor(code)} size="large" horizontal style={style}>{name}</Label>
    );
  }
}

export default StatusLabel;