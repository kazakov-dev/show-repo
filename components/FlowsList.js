import React from 'react';
import FlowItem from "./FlowItem";

class FlowsList extends React.Component {
  renderEmpty = () => (
    <div>
      Сейчас у вас нет активных потоков
    </div>
  );

  renderList() {
    const {flows, programs, statuses} = this.props;

    if (!statuses) {
      return null;
    }

    return flows.map((flow) => {
        const program = programs.find(program => program.id === flow.programId);
        const status = statuses.find(status => status.id === flow.flowStatusId);
        return {flow, program, status};
      })
      .map(({flow, program, status}) => <FlowItem {...{flow, program, status}} key={flow.id}/>);
  }

  render() {
    const {flows} = this.props;

    return (
      <div className="row">
        {flows.length > 0 ? (
          <div style={{marginTop: "20px"}}>
            {this.renderList()}
          </div>
        ) : (
          this.renderEmpty()
        )}
      </div>
    );
  }
}

export default FlowsList;