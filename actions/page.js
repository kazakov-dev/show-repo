import {setCollections} from './collections';

export const SET_UPDATE_STATE = 'NEED_UPDATE';

export const setUpdateState = (needUpdate) => ({
  type: SET_UPDATE_STATE,
  needUpdate
});

export const SET_MODERATOR = 'SET_MODERATOR';
export const setModerator = (moderator) => ({
  type: SET_MODERATOR,
  moderator
});

export const showListPage = ({collections, moderator}) => {
  return (dispatch) => {
    dispatch(setModerator(moderator));
    dispatch(setCollections(collections));
    dispatch(setUpdateState(false));
  }
};

