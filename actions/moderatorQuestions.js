import axios from 'axios';

export const REQUEST_MODERATOR_QUESTIONS = 'REQUEST_MODERATOR_QUESTIONS';
export const RECEIVE_MODERATOR_QUESTIONS = 'RECEIVE_MODERATOR_QUESTIONS';

export const requestModeratorQuestions = () => ({
  type: RECEIVE_MODERATOR_QUESTIONS,
  isFetching: true
});

export const fetchModeratorQuestions = (speakerOrderId) => {
  return dispatch => {
    dispatch(requestModeratorQuestions());

    axios
      .get(`/api/moderator/getQuestions?speakerOrderId=${speakerOrderId}`, {}, {
        withCredentials: true
      })
      .then(({data}) => dispatch(receiveModeratorQuestions(data)))
      .catch(err => console.log(err));
  };
};

export const receiveModeratorQuestions = ({listeners, questions}) => ({
  type: RECEIVE_MODERATOR_QUESTIONS,
  isFetching: false,
  listeners,
  questions
});

// Изменение статуса вопроса

export const SET_QUESTION_STATUS = 'SET_QUESTION_STATUS';

export const setQuestionStatus = (questionId, status) => ({
  type: SET_QUESTION_STATUS,
  questionId,
  status
});

export const sendQuestionStatus = (questionId, status) => {
  return dispatch => {
    dispatch(setQuestionStatus(questionId, status));

    axios
      .post('/api/moderator/setQuestionStatus/', {
        questionId, status
      }, {
        withCredentials: true
      })
      .then(({data}) => {})
      .catch(err => console.log(err));
  };
};