import axios from 'axios';

export const REQUEST_SENDING_QUESTION = 'REQUEST_SEND_QUESTION';
export const QUESTION_SENT = 'QUESTION_SENT';
export const ENABLE_EDITING = 'ENABLE_EDITING';
export const DISABLE_EDITING = 'DISABLE_EDITING';

export const enableEditing = () => ({
  type: ENABLE_EDITING,
  isEditing: true
});

export const disableEditing = () => ({
  type: DISABLE_EDITING,
  isEditing: false
});

export const requestSendingQuestion = () => ({
  type: REQUEST_SENDING_QUESTION,
  isSending: true
});

export const sendQuestion = (speakerOrderId, text) => {
  return (dispatch) => {
    dispatch(requestSendingQuestion());

    return axios.post('/api/questionforspeaker/update', {
      speakerOrderId,
      text
    }, {withCredentials: true})
      .then(response => dispatch(questionSent(response.data)))
      .catch(err => console.log(err));

  };
};

export const questionSent = ({text, status, id}) => ({
  type: QUESTION_SENT,
  isSending: false,
  isEditing: false,
  id,
  status,
  text
});


// Экшены для получения статуса задаваемого вопроса
export const REQUEST_QUESTION = 'REQUEST_QUESTION';
export const RECEIVE_QUESTION = 'RECEIVE_QUESTION';

export const requestQuestion = () => ({
  type: REQUEST_QUESTION,
  isUpdated: false
});

export const fetchQuestion = (speakerOrderId) => {
  return (dispatch, getState) => {
    dispatch(requestQuestion());

    return axios
      .get(`/api/questionForSpeaker/getInfo?speakerOrderId=${speakerOrderId}`,{
        withCredentials: true
      })
      .then(response => dispatch(receiveQuestion(response.data)))
      .catch(err => console.error(err));
  };
};

export const receiveQuestion = (data) => ({
  type: RECEIVE_QUESTION,
  isUpdated: true,
  ...data,
});



