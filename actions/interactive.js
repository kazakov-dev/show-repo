export const REQUEST_INTERACTIVE = 'REQUEST_INTERACTIVE';
export const RECEIVE_INTERACTIVE = 'RECEIVE_INTERACTIVE';

export const requestInteractive = () => ({
  type: REQUEST_INTERACTIVE,
  isUpdated: false
});

export const fetchInteractive = (flowId) => {
  return (dispatch, getState) => {
    const state = getState();
    const isUpdated = state.interactive.isUpdated;

    if (isUpdated) {
      return;
    }

    dispatch(requestInteractive());

    return fetch(`/api/interactive/getByFlowId?flowId=${flowId}`,{
        credentials: 'include'
      })
      .then(response => response.json())
      .then(data => dispatch(receiveInteractive(data)))
      .catch(err => console.error(err));
  };
};

export const receiveInteractive = (data) => ({
  type: RECEIVE_INTERACTIVE,
  isUpdated: true,
  ...data,
});
