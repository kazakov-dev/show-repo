import axios from 'axios';

export const REQUEST_LISTENERS_QUESTIONS = 'REQUEST_LISTENERS_QUESTIONS';
export const RECEIVE_LISTENERS_QUESTIONS = 'RECEIVE_LISTENERS_QUESTIONS';

export const requestListenersQuestions = () => ({
  type: REQUEST_LISTENERS_QUESTIONS,
  isFetching: true
});

export const fetchListenersQuestions = (speakerOrderId) => {
  return dispatch => {
    requestListenersQuestions();

    axios
      .get(
        `/api/listenersQuestions/getList?speakerOrderId=${speakerOrderId}`,
        {},
        {withCredentials: true}
      )
      .then(({data}) => dispatch(receiveListenersQuestions(data)));
  };
};

export const receiveListenersQuestions = (data) => ({
  type: RECEIVE_LISTENERS_QUESTIONS,
  isFetching: false,
  ...data
});


// Голосование пользователей
export const REQUEST_TOGGLE_VOTE = 'REQUEST_TOGGLE_VOTE';
export const RECEIVE_TOGGLE_VOTE = 'RECEIVE_TOGGLE_VOTE';

export const requestToggleVote = () => ({
  type: REQUEST_TOGGLE_VOTE,
  isFetchingVote: true
});

export const toggleVote = (questionForSpeakerId, speakerOrderId) => {
  return dispatch => {
    dispatch(requestToggleVote());

    axios
      .post('/api/vote/toggle', {questionForSpeakerId, speakerOrderId}, {withCredentials: true})
      .then(({data}) => dispatch(receiveToggleVote(data)));
  };
};

export const receiveToggleVote = ({votes}) => ({
  type: RECEIVE_TOGGLE_VOTE,
  isFetchingVote: false,
  votes
});