export const REQUEST_DIRECTORIES = 'REQUEST_DIRECTORIES';
export const RECEIVE_DIRECTORIES = 'RECEIVE_DIRECTORIES';

export const requestDirectories = () => ({
  type: REQUEST_DIRECTORIES,
  isUpdated: false
});

export const fetchDirectories = () => {
  return (dispatch, getState) => {
    const state = getState();
    const isUpdated = state.directories.isUpdated;

    if (isUpdated) {
      return;
    }

    dispatch(requestDirectories());

    return fetch(`/api/interactive/getDirectories`, {
      credentials: 'include'
    })
      .then(response => response.json())
      .then(directories => dispatch(receiveDirectories(directories)))
      .catch(err => console.error(err));
  };
};

export const receiveDirectories = (directories) => ({
  type: RECEIVE_DIRECTORIES,
  ...directories,
  isUpdated: true
});