import axios from 'axios';
import {fetchOnline} from './online';

export const PREPARE_SPEAKER_STATUS_CHANGE = 'PREPARE_SPEAKER_STATUS_CHANGE';
export const REVERT_SPEAKER_STATUS_CHANGE = 'REVERT_SPEAKER_STATUS_CHANGE';
export const REQUEST_SPEAKER_STATUS_CHANGE = 'REQUEST_SPEAKER_STATUS_CHANGE';
export const RECEIVE_SPEAKER_STATUS_CHANGE = 'RECEIVE_SPEAKER_STATUS_CHANGE';

export const prepareSpeakerStatusChange = (prepareSpeakerOrderId, prepareSpeakerOrderStatusId) => ({
  type: PREPARE_SPEAKER_STATUS_CHANGE,
  prepareSpeakerOrderId,
  prepareSpeakerOrderStatusId
});

export const revertSpeakerStatusChange = () => ({});

export const requestSpeakerStatusChange = () => ({
  type: REQUEST_SPEAKER_STATUS_CHANGE,
  isFetching: true
});

export const postSpeakerStatusChange = (speakerOrderId, speakerOrderStatusId) => {
  return (dispatch, getState) => {
    const state = getState();
    const flowId = state.interactive.flow.id;
    dispatch(requestSpeakerStatusChange());

    return axios.post('/api/moderator/updateSpeakerOrder', {
      speakerOrderId,
      speakerOrderStatusId,
      flowId
    }, {
      withCredentials: true
    }).then(response => {
      dispatch(receiveSpeakerStatusChange());
      dispatch(fetchOnline(flowId));
    });
  }
};

export const receiveSpeakerStatusChange = () => ({
  type:RECEIVE_SPEAKER_STATUS_CHANGE,
  isFetching: false,
  prepareSpeakerOrderId: 0,
  prepareSpeakerOrderStatusId: 0
});