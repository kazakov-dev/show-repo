export const SET_COLLECTIONS = 'SET_COLLECTIONS';

export const setCollections = ({programs, flows}) => ({
  type: SET_COLLECTIONS,
  programs,
  flows
});