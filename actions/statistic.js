import axios from 'axios';

export const REQUEST_STATISTIC = 'REQUEST_STATISTIC';
export const RECEIVE_STATISTIC = 'RECEIVE_STATISTIC';

export const requestStatistic = () => ({
  type: REQUEST_STATISTIC,
  isFetching: true,
  items: []
});
export const fetchStatistic = (flowId) => {
  return (dispatch) => {
    dispatch(requestStatistic());

    axios
      .get('/api/statistic/getInfo', {
        params: {flowId}
      }, {
        withCredentials: true
      })
      .then(({data}) => dispatch(receiveStatistic(data)))
      .catch(err => console.log(err));
  };
};

export const receiveStatistic = ({items}) => ({
  type: RECEIVE_STATISTIC,
  isFetching: false,
  items
});