import axios from 'axios';

export const REQUEST_ONLINE = 'REQUEST_ONLINE';
export const RECEIVE_ONLINE = 'RECEIVE_ONLINE';

export const requestOnline = () => ({
  type: REQUEST_ONLINE,
  isFetching: true
});

export const fetchOnline = (flowId) => {
  return (dispatch) => {
    dispatch(requestOnline());

    return axios.get(`/api/online/getInfo?flowId=${flowId}`,
      {
        withCredentials: true,
      })
      .then(response => dispatch(receiveOnline(response.data)))
      .catch(err => console.error(err));
  };
};

export const receiveOnline = (data) => ({
  type: RECEIVE_ONLINE,
  isFetching: false,
  ...data,
});

// answers

const CancelToken = axios.CancelToken;
let cancelToken = null;

export const REQUEST_ANSWER_UPDATE = 'REQUEST_ANSWER_UPDATE';
export const ANSWER_UPDATED = 'ANSWER_UPDATED';

export const requestAnswerUpdate = (answers, questionId) => ({
  type: REQUEST_ANSWER_UPDATE,
  isAnswersSending: true,
  answers,
  questionId
});

export const postAnswer = (answers, questionId) => {
  if (!Array.isArray(answers)) {
    answers = [answers];
  }

  if (cancelToken) {
    cancelToken.cancel();
  }

  cancelToken = CancelToken.source();

  return (dispatch, getState) => {
    const state = getState();
    const flow = state.interactive.flow;

    dispatch(requestAnswerUpdate(answers, questionId));

    return axios
      .post('/api/answer/update', {answers, questionId}, {
        withCredentials: true,
        cancelToken: cancelToken.token
      }).then((response) => {
        dispatch(answerUpdated());
        dispatch(fetchOnline(flow.id)); // Отправляем запрос на обновление страницы
      })
      .catch(err => {
        if (!axios.isCancel(err)) {
          console.error(err);
        }
      });
  }
};

export const answerUpdated = () => ({
  type: ANSWER_UPDATED,
  isAnswersSending: false
});

// Модерация

export const SET_SPEAKER = 'SET_SPEAKER';

export const setSpeaker = (speakerOrderId) => ({
  type: SET_SPEAKER,
  speakerOrderId
});

